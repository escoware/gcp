import os
import io
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
#from sklearn.externals import joblib
from joblib import load

from google.cloud import storage
client = storage.Client()


def audit_model(data, context):
    file_data = data

    file_name = file_data['name']
    bucket_name = file_data['bucket']
    model_name = os.path.basename(file_name)

    if file_name[-4:] != '.pkl':
        print(file_name, 'not a pickle')
        return

    blob = client.bucket(bucket_name).get_blob(file_name)
    print(blob, file_name[-4:])

    weather_forecast_file = 'gs://ml-barnum/csv/ERCOT_WEATHER_DOUBLE_PIVOT_FORECAST.csv'
    df = pd.read_csv(weather_forecast_file)

    f = io.BytesIO()
    blob.download_to_file(f)
#    models = joblib.load(f)
    models = load(f)

    pipeline = ColumnTransformer(
        [("num", StandardScaler(), slice(1, -1)), ("cat", OneHotEncoder(), [0]), ])
    output = pd.DataFrame([])

    # for each model
    for key, model in models.items():
        print('Predict', key)
        reg, X_train = model
        print('Columns:', X_train.columns)
        X_test = df[X_train.columns]
        X_test = X_test.dropna(axis=1)

        # fit pipeline and scale test data
        pipeline.fit(X_train)
        X_test = pipeline.transform(X_test)
        y_pred = reg.predict(X_test)
        print(key, y_pred.shape, file_name)

        # clumsy
        df['Pred'] = y_pred
        df_out = pd.DataFrame(df[['CalendarDate_Time', 'Pred']])
        df_out['CustomerId'], df_out['Zone'] = key
        df_out['Model'] = model_name[:-4]
        output = output.append(df_out, ignore_index=True)

    out_file = 'gs://ml-barnum/forecast/predict.' + model_name + '.csv'
    output.to_csv(out_file, index=False)
