/**
 * sends email using mailgun's https api
 * @param {!Object} req Cloud Function request context.
 * @param {!Object} res Cloud Function response context.
 */

exports.sendEmails = function sendEmailFunction(req, res) {
    
    var request = require("request");
    var api_key = 'key-a280159e370fca6389f4235c1cdaa4f3';
    var domain = 'mg.powwr.com';
    var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

    var files = [];
    if(typeof req.body.attachmentUrls != "undefined")
    {
        for (var i = 0; i < req.body.attachmentUrls.length; i++) {
            files[i] = request(req.body.attachmentUrls[i]);
        }
    }
    var emailData = {
        from: "noreply@powwr.com",
        to: req.body.to,
      	cc:req.body.cc,
        subject: req.body.subject,
        text: req.body.text,
        attachment:files
    
      };
      
     mailgun.messages().send(emailData,function(error, body) {
        if (error) {
            console.log(error);
          res.status(500).send('Error: ' + error);
        }
        else{
           console.log(body);
         res.status(200).send('Success');
        }
    }
);
      
    };



    
    /* Test data
     {
        "to":"cpond@powwr.com",
        "cc":"cpond@escoadvisors.com",
        "text":"body of email",
        "subject":"my subject line",
        "attachmentUrls":
        [
            "https://www.google.ca/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
            "https://www.google.ca/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
        ]    
    }
    
    */