/**
 * Responds to any HTTP request that can provide a "message" field in the body.
 *
 * @param {!Object} req Cloud Function request context.
 * @param {!Object} res Cloud Function response context.
 */
exports.sendNotification = (req, res) => {
 
    var request = require("request");
   
  /*var requestData = {
        "to":"cpond@powwr.com",
      	"cc":"cpond@powwr.com",
    	"subject": "Daily Gas DDR Data",
    	"text":"Attached is the Gas DDR report",
       "attachmentUrls":
        [
           "https://storage.googleapis.com/ddr-daily-delivery/LexingtonDDRsOutbound.csv"
        ]    
        }*/
  
  
	var requestData = {
        "to":"dj@powwr.com,jbassett@powwr.com",
      	"cc":"cpond@powwr.com,bcascone@powwr.com,acable@powwr.com,dyegidis@powwr.com,lkoen@powwr.com,mparrella@powwr.com",
    	"subject": "Daily Gas DDR Data",
    	"text":"Attached is the Gas DDR report",
       "attachmentUrls":
        [
           "https://storage.googleapis.com/ddr-daily-delivery/LexingtonDDRsOutbound.csv"
        ]    
        }
  
    var url="https://us-central1-gas-ddr.cloudfunctions.net/httpFunc-SendEmail";
    
  request({
    url: url,
    method: "POST",
    json: requestData
}, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log(body);
            res.status(200).send('Success');
        }
        else {
            console.error("error: " + error)
            res.status(500).send('Error');
        }
    })   
};

/*
https://storage.googleapis.com/ddr-daily-delivery/LexingtonDDRsOutbound.csv

*/
