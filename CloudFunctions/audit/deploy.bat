: %1 = function suffix / bucket suffix (000-dmz-files)
gcloud functions deploy audit-%1 --entry-point audit --runtime python37 --trigger-resource gs://%1 --trigger-event google.storage.object.finalize --retry --timeout=90s --memory=1024MB --set-env-vars LOG_SUFFIX=.audit.txt
