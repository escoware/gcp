import os
import io
import numpy as np
import pandas as pd

from collections import Counter

from google.cloud import storage
client = storage.Client()


def audit(data, context):
    file_data = data

    file_name = file_data['name']
    bucket_name = file_data['bucket']

    blob = client.bucket(bucket_name).get_blob(file_name)
    print(blob)

    print('byte histogram below')
    s = blob.download_as_string().decode('ASCII')
    wordcount = Counter(s)

    for k in wordcount.keys():
        print(k, k.encode('utf-8').hex(), wordcount[k])

    sio = io.StringIO(s)
    df = pd.read_csv(sio)
    print(df.info())
