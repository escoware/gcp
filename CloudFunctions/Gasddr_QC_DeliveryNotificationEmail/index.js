/**
 * Responds to any HTTP request that can provide a "message" field in the body.
 *
 * @param {!Object} req Cloud Function request context.
 * @param {!Object} res Cloud Function response context.
 */
exports.sendNotification = (req, res) => {
 
    var request = require("request");
   
	// Loop through each receipient and do the following

   
  
	var requestData = {
        "to":"bcascone@powwr.com",
        "cc":"lkoen@powwr.com",
        "subject": "Daily Gas DDR QC Data",
    	"text":"Attached is the Gas DDR data QC report",
      "attachmentUrls":
        [
           "https://storage.googleapis.com/ddr-daily-delivery/Gas_ddr_ConEdDDR_QC_Stage1_Results.csv",
          "https://storage.googleapis.com/ddr-daily-delivery/Gas_ddr_ConEdDDR_QC_Data_Results.csv",
          "https://storage.googleapis.com/ddr-daily-delivery/Gas_ddr_NationalGrid_QC_Data_Results.csv",
          "https://storage.googleapis.com/ddr-daily-delivery/Gas_ddr_NationalGrid_QC_Stage1_Results.csv"
          
        ]   
        }
  
    var url="https://us-central1-gas-ddr.cloudfunctions.net/httpFunc-SendEmail";
    
  request({
    url: url,
    method: "POST",
    json: requestData
}, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log(body);
            res.status(200).send('Success');
        }
        else {
            console.log("error: " + error)
            res.status(500).send('Error');
        }
    })   
};

