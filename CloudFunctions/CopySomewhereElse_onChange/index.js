exports.CopySomewhereElse_onChange = (event, callback) => {
  const file = event.data;
  const context = event.context;

  const Storage = require('@google-cloud/storage');

  // Creates a client
  const storage = new Storage();

  const srcBucketName = file.bucket;
  const srcFilename = file.name;
  const destBucketName = 'moved_on_change_dest';
  
  const destFilename = file.name;
  const filesize = file.size;
  const FileName = file.name;
  const FiletimeModified = file.LastModified;
  const FileID = file.ID;
  const FiletimeCreated = file.timeCreated;

  
   const generation = file.Generation;
  const generationmatch = file.ifGenerationMatch;
  const generationNotMatch = file.ifGenerationNotMatch;
  
  const metagenerationMatch = file.ifMetagenerationMatch;
  const metagenerationNotMatch = file.ifMetagenerationNotMatch;
  
  //const fileContentDisposition = file.contentDisposition;
  
  
 
                                // Copies the file to the other bucket
                                storage
                                  .bucket(srcBucketName)
                                  .file(srcFilename)
                                  .copy(storage.bucket(destBucketName).file(destFilename))
  								file
                                  .Generation(srcFilename)
  							   //.get(storage.bucket(destBucketName).file(destFilename))
                                  .then(() => {
                                    console.log
                                              (
                                                  `gs://${srcBucketName}/${srcFilename} copied to gs://${destBucketName}/${destFilename}.|${filesize}|${FileName}|${FiletimeCreated}|${srcBucketName}|${generation}`
                                              )
                                          //	(
                                          //		`${filesize}`
                                          //	)
                                  ;
                                  })
                                  .catch(err => {
                                    console.error('ERROR:', err);
                                  });

                                callback();
                              };
 