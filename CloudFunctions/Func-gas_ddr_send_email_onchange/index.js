exports.sendIfDiff = (event, callback) => {
  const file = event.data;
  const context = event.context;
  var readline = require('readline')
  const Storage = require('@google-cloud/storage');
    const storage = new Storage();  
  
  
    const options = {
      input: getFileStream(file)
       
    };
  


  var totalLines = 0; 
  var rl = readline.createInterface(options);
  
  rl.on('line', function (line) {
    totalLines++;
     console.log("Reading line" + totalLines);
  });
  
  rl.on('close', function () {
    if (totalLines> 1)
    {
      sendEmail();
      console.log("We should send email here")
    }
    else
    {
      console.log("No email required")
    }
    callback();
  });
}

function getFileStream (file) {
  console.info('In getFileStream')
  const Storage = require('@google-cloud/storage');
  // Creates a client
  const storage = new Storage();  
  
  if (!file.bucket) {
    throw new Error('Bucket not provided. Make sure you have a "bucket" property in your request');
  }
  if (!file.name) {
    throw new Error('Filename not provided. Make sure you have a "name" property in your request');
  }
  console.info('About to return file stream')
  return storage.bucket(file.bucket).file(file.name).createReadStream();
}

function sendEmail () {
  console.log("In SendEmail")
  const request = require('request');
  var requestData = {
    "to":"dj@powwr.com,jbassett@powwr.com",
    "cc":"cpond@powwr.com,bcascone@powwr.com,acable@powwr.com,dyegidis@powwr.com,lkoen@powwr.com,mparrella@powwr.com",
    "subject": "Daily Gas DDR Data",
    "text":"Attached is the Gas DDR report",
    "attachmentUrls":[
    "https://storage.googleapis.com/gas_ddr_dailydelivery_onchange_send/LexingtonDDRsOutbound_onChange.csv"
    ]
      };

var url="https://us-central1-gas-ddr.cloudfunctions.net/httpFunc-SendEmail";
return request({url: url, method: "POST", json: requestData});
}