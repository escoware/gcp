:Example - this.bat amerigreen 10GB ftp2.ecinfosystems.com txt EscoAdvisor_Amrgreen nunya
set ZONE=us-east1-b
set PROJECT=bit-harbor

set SERVER=%1
set SIZE=%2
set SITE=%3
set EXT=%4
set USER=%5
set PASS=%6

gcloud compute instances create %SERVER%^
 --project=%PROJECT%^
 --zone=%ZONE%^
 --machine-type=n1-standard-1^
 --subnet=default^
 --metadata=startup-script-url=gs://dmz_files/SaveAllTheThings,ftp_user=%USER%,ftp_password=%PASS%,SITE=%SITE%,EXT=%EXT%^
 --no-restart-on-failure^
 --maintenance-policy=TERMINATE^
 --preemptible^
 --service-account=tugboat@bit-harbor.iam.gserviceaccount.com^
 --scopes=https://www.googleapis.com/auth/cloud-platform^
 --min-cpu-platform=Automatic^
 --image=ubuntu-1804-bionic-v20181029^
 --image-project=ubuntu-os-cloud^
 --boot-disk-size=%SIZE%^
 --boot-disk-type=pd-standard^
 --boot-disk-device-name=%SERVER%
