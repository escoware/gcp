USE [master]
GO

DROP PROCEDURE [dbo].[FILL_DFSU_TABLES]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FILL_DFSU_TABLES]
 @DB Varchar(255)
,@Path Varchar(255) = 'D:\BackupsFromProc\'

AS
BEGIN
set nocount on
exec FILL_DFSU_TABLE @DB,'HourlyForecast',@Path
exec FILL_DFSU_TABLE @DB,'HourlyBackcast',@Path
exec FILL_DFSU_TABLE @DB,'DailyCumulativemWh',@Path

END
GO


