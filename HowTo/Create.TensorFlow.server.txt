gcloud beta compute instances create test-1^
 --project=husky-powwr-217818^
 --zone=us-east1-b^
 --machine-type=n1-standard-1^
 --subnet=default^
 --network-tier=PREMIUM^
 --maintenance-policy=MIGRATE^
 --service-account=husky-power-service-account@husky-powwr-217818.iam.gserviceaccount.com^
 --scopes=https://www.googleapis.com/auth/cloud-platform^
 --image=c2-deeplearning-tf-1-11-cu100-20181008^
 --image-project=ml-images^
 --boot-disk-size=30GB^
 --boot-disk-type=pd-standard^
 --boot-disk-device-name=test-1

: preemptible version of above

gcloud beta compute instances create test-1^
 --project=husky-powwr-217818^
 --zone=us-east1-b^
 --machine-type=n1-standard-1^
 --subnet=default^
 --network-tier=PREMIUM^
 --no-restart-on-failure^
 --maintenance-policy=TERMINATE^
 --preemptible^
 --service-account=husky-power-service-account@husky-powwr-217818.iam.gserviceaccount.com^
 --scopes=https://www.googleapis.com/auth/cloud-platform^
 --image=c2-deeplearning-tf-1-11-cu100-20181008^
 --image-project=ml-images^
 --boot-disk-size=30GB^
 --boot-disk-type=pd-standard^
 --boot-disk-device-name=test-1
