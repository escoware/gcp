create database Dfsu_PPLElectric
create database Quad_ACE
create database Dfsu_NIMO_B
create database Quad_BHEC
create database Dfsu_CONED_I
create database Dfsu_PotomacEd
create database Dfsu_JCPL_A
create database Quad_NYSEG_G
create database Dfsu_UES
create database Dfsu_NIMO_C
create database Dfsu_UI
create database Quad_ERCOT_EAST
create database Quad_WMECO_A
create database Dfsu_WPP
create database Quad_NYSEG_C2
create database Quad_PSEG_A
create database Quad_NGUS
create database Quad_ERCOT_WEST
create database Quad_NYSEG_A
create database Dfsu_OE
create database Quad_DUQ
create database Quad_NYSEG_E2
create database Quad_NGMA_WCMA_7D
create database Quad_NGMA_NEMA_8B
create database Dfsu_DUKE
create database Dfsu_TE
create database Quad_NSTAR_ComElec
create database Quad_PEPCO_DC
create database Quad_CLP
create database Quad_ColumbusSouthern
create database Quad_NHEC
create database Dfsu_NIMO_A
create database Quad_NYSEG_F
create database Dfsu_Penelec
create database Quad_CenHud
create database Quad_ERCOT_SOUTH
create database Quad_ERCOT_COAST
create database TEST_CONED_J
create database Quad_ERCOT_FWEST
create database Dfsu_CEI
create database Dfsu_NIMO_F1
create database Quad_METED
create database Quad_ERCOT_SCENT
create database Dfsu_NGRI
create database Quad_NSTAR_BostonEd
create database Quad_ERCOT_NORTH
create database Dfsu_Delmarva_MD_A
create database Quad_Delmarva_DE
create database Quad_RGE
create database Quad_NYSEG_D
create database Silo_DAYTON
create database Dfsu_NIMO_D
create database Quad_COMED
create database Dfsu_PennPower
create database Dfsu_CONED_J
create database Quad_ERCOT_NCENT
create database Quad_NSTAR_Cambridge
create database Dfsu_NIMO_E1
create database Cube_PSNH
create database Dfsu_Fitchburg
create database Quad_NYSEG_B
create database Quad_BGE
create database Quad_PEPCO_MD
create database Quad_PECO
create database Quad_OHIOPOWER
create database Quad_OR
create database Quad_NGMA_SEMA_6C
create database Quad_CMP
create database Quad_ROCKLAND
create database Quad_NGMA_SEMA_6A
