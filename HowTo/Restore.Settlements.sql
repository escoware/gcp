RESTORE DATABASE ERCOT_Settlements FROM  DISK = N'D:\BackupsFromProc\ERCOT_Settlements.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE HedgeDFS FROM  DISK = N'D:\BackupsFromProc\HedgeDFS.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE ISONE_Settlements FROM  DISK = N'D:\BackupsFromProc\ISONE_Settlements.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE ISO_Data FROM  DISK = N'D:\BackupsFromProc\ISO_Data.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE NYISO_Settlements FROM  DISK = N'D:\BackupsFromProc\NYISO_Settlements.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE PJM_Settlements FROM  DISK = N'D:\BackupsFromProc\PJM_Settlements.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
