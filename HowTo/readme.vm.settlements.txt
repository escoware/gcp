
set SERVER=settlements
set ZONE=us-east1-b
set PROJECT=dev-settlements

1. run C:\gcp\HowTo\Create.dbserver.settlements.bat
2. create a login, login, change password (gcloud compute reset-windows-password --project dev-settlements --user oz settlements)
3. assign external ip address - 35.231.79.191 - to VM settlements
4. disk management
- mount D, S

gcloud components update

md S:\Escoware
md S:\SQLData\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\HEDGEDFS
md S:\SQLData\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\ISONE
md S:\SQLData\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\ISO_DATA
md S:\SQLData\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\NYISO

gsutil -m rsync -r gs://bin-settlements s:\Escoware

md D:\BackupsFromProc
gsutil -m rsync -r gs://armadillo D:\BackupsFromProc

5. - SQL Server Setup
- a. enable sql server access

- b. Restore.Settlements.sql
RESTORE DATABASE ERCOT_Settlements FROM  DISK = N'D:\BackupsFromProc\ERCOT_Settlements_backup_2018_05_01_050011_0433210.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE HedgeDFS FROM  DISK = N'D:\BackupsFromProc\HedgeDFS_backup_2018_05_01_050011_2222494.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE ISO_Data FROM  DISK = N'D:\BackupsFromProc\ISO_Data_backup_2018_05_01_050011_2302462.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE NYISO_Settlements FROM  DISK = N'D:\BackupsFromProc\NYISO_Settlements_backup_2018_05_01_050011_2582350.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11
RESTORE DATABASE PJM_Settlements FROM  DISK = N'D:\BackupsFromProc\PJM_Settlements_backup_2018_05_01_050011_2652322.bak' WITH  FILE = 1,  NOUNLOAD,  STATS = 11

- c. Logins for escoware, acable, gculver
CREATE LOGIN escoware WITH PASSWORD = 'abcd', CHECK_POLICY = OFF ;  
ALTER SERVER ROLE [bulkadmin] ADD MEMBER [escoware]
ALTER SERVER ROLE [dbcreator] ADD MEMBER [escoware]
ALTER SERVER ROLE [diskadmin] ADD MEMBER [escoware]
ALTER SERVER ROLE [processadmin] ADD MEMBER [escoware]
ALTER SERVER ROLE [securityadmin] ADD MEMBER [escoware]
ALTER SERVER ROLE [serveradmin] ADD MEMBER [escoware]
ALTER SERVER ROLE [setupadmin] ADD MEMBER [escoware]
ALTER SERVER ROLE [sysadmin] ADD MEMBER [escoware]
