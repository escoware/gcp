set SERVER=gexa
set ZONE=us-east1-b
set PROJECT=bit-harbor

call gcloud compute instances create %SERVER%^
 --project=%PROJECT%^
 --zone %ZONE%^
 --service-account=tugboat@bit-harbor.iam.gserviceaccount.com^
 --scopes=https://www.googleapis.com/auth/cloud-platform^
 --image-project windows-sql-cloud^
 --image-family sql-web-2017-win-2016^
 --machine-type  n1-standard-4^
 --boot-disk-size 200^
 --boot-disk-type pd-standard

: --no-restart-on-failure^
: --maintenance-policy=TERMINATE^
: --preemptible^

:d drive (file transfer)
call gcloud compute --project=%PROJECT% disks create blank-40 --zone=%ZONE% --type=pd-standard --size=40GB
call gcloud compute instances attach-disk %SERVER% --zone %ZONE% --disk blank-40

:s drive (database files)
call gcloud compute --project=%PROJECT% disks create s-300 --zone=%ZONE% --type=pd-standard --size=300GB
call gcloud compute instances attach-disk %SERVER% --zone %ZONE% --disk s-300
