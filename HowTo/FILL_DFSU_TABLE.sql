USE [master]
GO

DROP PROCEDURE [dbo].[FILL_DFSU_TABLE]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FILL_DFSU_TABLE]
 @DB Varchar(255)
,@Table Varchar(255)
--,@File Varchar(255) = @DB+'.'+@Table+'.csv'
,@Path Varchar(255) = 'D:\BackupsFromProc\'

AS
BEGIN
declare @Start datetime = getdate()
declare @cmd nvarchar(4000)

declare @File varchar(255) = @DB+'.'+@Table+'.csv'

set @cmd = N'TRUNCATE TABLE ' + @DB+'.dbo.'+@Table
print @cmd
execute sp_executesql @cmd

set @cmd = N'BULK INSERT ' + @DB+'.dbo.'+@Table + ' FROM ''' + @Path+@File + ''' WITH ( FIELDTERMINATOR = '','', FIRSTROW = 2, ROWTERMINATOR = ''\n'' )'

print @cmd
execute sp_executesql @cmd

END
GO
