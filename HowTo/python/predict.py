import numpy as np
import pandas as pd
import glob
import os
import sys
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.externals import joblib


def load_data(csv_path):
    return pd.read_csv(csv_path)


def make_pipeline():
    return ColumnTransformer([("num", StandardScaler(), slice(1, -1)), ("cat", OneHotEncoder(), [0]), ])


def main():

    if len(sys.argv) < 3:
        print('To few arguments, please specify input file and output file')
        sys.exit(2)

    test_csv_path = sys.argv[1]
    output_file = sys.argv[2]

    # make pipeline
    pipeline = make_pipeline()

    output = pd.DataFrame([])

    # load testing data
    print("Loading testing data...")
    df = load_data(test_csv_path)
    print("Testing data successfully loaded")

    piclkes = glob.glob('*.pkl', recursive=True)
    for models_path in piclkes:
        model_name = os.path.basename(models_path)

        if model_name[:3] == 'pca':
            print('Ignoring '+models_path)
            continue

        # load models
        print("Loading pre-trained models from "+models_path+' ...')
        models = joblib.load(models_path)
        print("Models successfully loaded")

        # for each model
        for key, model in models.items():
            print('Predict', key)
            reg, X_train = model
            print('Columns:', X_train.columns)
            X_test = df[X_train.columns]
            X_test = X_test.dropna(axis=1)

            # fit pipeline and scale test data
            pipeline.fit(X_train)
            X_test = pipeline.transform(X_test)
            y_pred = reg.predict(X_test)
            print(key, y_pred.shape, models_path)

            # clumsy
            df['Pred'] = y_pred
            df_out = pd.DataFrame(df[['CalendarDate_Time', 'Pred']])
            df_out['CustomerId'], df_out['Zone'] = key
            df_out['Model'] = model_name[:-4]
            output = output.append(df_out, ignore_index=True)

    output.to_csv(output_file, index=False)


if __name__ == '__main__':
    main()
