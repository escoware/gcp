sudo mkdir -p /py
sudo chmod a+w /py
sudo mkdir -p /py/bin
sudo chmod a+w /py/bin
INSTANCE=$(uname -n)
CUSTOMER=$(gcloud compute instances describe ${INSTANCE} --zone=us-east1-b --format="value(metadata.items[CUSTOMER])")
gsutil cp gs://ml-barnum/py/*attr*.py /py/bin
cd /py/bin/
rm work.txt

for zone in ercot_n ercot_s ercot_h ercot_w
do
  for py in *attr*.py
  do
    echo python3 $py  ${CUSTOMER} $zone gs://ml-barnum/csv/ >>work.txt
    echo vmstat -s >>work.txt
  done
done

gsutil cp gs://ml-barnum/CloudTasks /py/bin
chmod +x /py/bin/CloudTasks
/py/bin/CloudTasks -Lanes 21 -LogName ${INSTANCE} -Project husky-powwr-217818 -Work work.txt
gsutil cp /py/bin/*.pkl gs://ml-barnum/pickles/
gcloud compute instances stop ${INSTANCE} --zone=us-east1-b
#gcloud compute instances delete ${INSTANCE} --zone=us-east1-b
