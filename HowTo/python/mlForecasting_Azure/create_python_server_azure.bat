:Example - this.bat 15
set LOCATION=eastus2
set RESOURCEGROUP=husky-powwr-217818

set SERVER=train-%1
set SIZE=30GB
set CUSTOMER=%1

az vm create --name %SERVER% --resource-group %RESOURCEGROUP% --location %LOCATION%  --size Standard_D8s_v3 --custom-data cloud-init.sh --image microsoft-ads:linux-data-science-vm-ubuntu:linuxdsvmubuntu:19.05.01 --os-disk-size %SIZE% --os-disk-name %SERVER% --tags 'CUSTOMER=%1' 
             
Set-Variable -Name "LOCATION" -Value "eastus2"
Set-Variable -Name "RESOURCEGROUP" -Value "husky-powwr-217818"
Set-Variable -Name "SERVER" -Value "train"
