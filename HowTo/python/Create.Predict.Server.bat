:Example - this.bat 15
set ZONE=us-east1-b
set PROJECT=husky-powwr-217818

set SERVER=predict
set SIZE=200GB


gcloud compute instances create %SERVER%^
 --project=%PROJECT%^
 --zone=%ZONE%^
 --machine-type=n1-standard-8^
 --subnet=default^
 --metadata=startup-script-url=gs://ml-barnum/Predict^
 --no-restart-on-failure^
 --maintenance-policy=TERMINATE^
 --preemptible^
 --service-account=141106379918-compute@developer.gserviceaccount.com^
 --scopes=https://www.googleapis.com/auth/cloud-platform^
 --min-cpu-platform=Automatic^
 --image=c2-deeplearning-tf-1-13-cu100-20190514^
 --image-project=ml-images^
 --boot-disk-size=%SIZE%^
 --boot-disk-type=pd-standard^
 --boot-disk-device-name=%SERVER%
