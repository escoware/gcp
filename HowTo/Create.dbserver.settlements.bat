set SERVER=settlements
set ZONE=us-east1-b
set PROJECT=dev-settlements

: Create disks before VM to ensure disks are available
call gcloud compute --project=%PROJECT% disks create blank-30 --zone=%ZONE% --type=pd-standard --size=30GB
call gcloud compute --project=%PROJECT% disks create s-200    --zone=%ZONE% --type=pd-standard --size=200GB

: create VM, attach disks
call gcloud compute instances create      %SERVER% --project=%PROJECT% --zone %ZONE% --image-project windows-sql-cloud --image-family sql-web-2017-win-2016 --machine-type  n1-standard-4 --boot-disk-size 60 --boot-disk-type pd-standard
call gcloud compute instances attach-disk %SERVER% --project=%PROJECT% --zone %ZONE% --disk blank-30
call gcloud compute instances attach-disk %SERVER% --project=%PROJECT% --zone %ZONE% --disk s-200
