set YYYY_MM_DD=2019_03_06

call gsutil -m cp gs://bak-settlements/ERCOT_Settlements*%YYYY_MM_DD%* gs://armadillo/ERCOT_Settlements.bak
call gsutil -m cp gs://bak-settlements/HedgeDFS*%YYYY_MM_DD%* gs://armadillo/HedgeDFS.bak
call gsutil -m cp gs://bak-settlements/ISONE_Settlements*%YYYY_MM_DD%* gs://armadillo/ISONE_Settlements.bak
call gsutil -m cp gs://bak-settlements/ISO_Data*%YYYY_MM_DD%* gs://armadillo/ISO_Data.bak
call gsutil -m cp gs://bak-settlements/NYISO_Settlements*%YYYY_MM_DD%* gs://armadillo/NYISO_Settlements.bak
call gsutil -m cp gs://bak-settlements/PJM_Settlements*%YYYY_MM_DD%* gs://armadillo/PJM_Settlements.bak
