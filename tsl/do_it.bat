time <crlf
copy /Y \tsl\data\output\Model_Daily_Mape.header.csv \tsl\data\output\Model_Daily_Mape.csv
copy /Y \tsl\data\output\Model_predictions.header.csv \tsl\data\output\Model_predictions.csv

python build_model\tsl_Train_Test.py Clearview.csv ignore.csv Experiment.csv Files.csv METED.csv
python build_model\tsl_Train_Test.py Clearview.csv ignore.csv Experiment.csv Files.csv COMED.csv
python build_model\tsl_Train_Test.py Eligo.csv ignore.csv Experiment.csv Files.csv METED.csv
python build_model\tsl_Train_Test.py Eligo.csv ignore.csv Experiment.csv Files.csv COMED.csv

python query_monthly_model.py
python query_monthly_blowout.py

python query_csv_model.py
python query_csv_blowout.py
time <crlf
