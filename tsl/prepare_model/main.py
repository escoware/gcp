import os
from google.cloud import pubsub_v1
from datetime import datetime
import pandas as pd

TOPIC = os.getenv('TOPIC')
TRACK = os.getenv('TRACK')
MAP = os.getenv('MAP')
WORTHY = os.getenv('WORTHY')

PS = pubsub_v1.PublisherClient()


def Track(message_id, my_name, status):
    # when is more important than who
    asof = str(datetime.now().timestamp())
    message = 'track,{},{},{}\n'.format(message_id, asof, status)
    print(message)
    PS.publish(TRACK, message.encode('utf-8'))


def check_worthy(df, Supplier, LoadZone, Utility, WeatherZone):
    wz = df[(df['WeatherZone'].str.lower() == WeatherZone.lower())]
    ut = wz[(wz['Utility'].str.lower() == Utility.lower())]
    lz = ut[(ut['LoadZone'].str.lower() == LoadZone.lower())]
    su = lz[(lz['Supplier'].str.lower() == Supplier.lower())]

    return len(su)


def Publish_Model_request(config, message, Supplier, LoadZone, Utility, WeatherZone, WeatherStation, MinDate, MaxDate, ActualDays, data_folder):
    TrainingSuppliers = lookup(config, 'TrainingSuppliers', Supplier)
    print('debug', Supplier, LoadZone, Utility,
          WeatherZone, WeatherStation, MinDate, MaxDate, 'ActualDays=', ActualDays)

    ISO = lookup(config, 'ISO', 'ERCOT')
    ExperimentID = lookup(config, 'ExperimentID', '1')
    ActualDelay = lookup(config, 'ActualDelay', '7')
    MinLeaf = lookup(config, 'MinLeaf', '60')
    Features = lookup(config, 'Features', 'Hour~Temperature~DewPoint~Humidity')
    feature_file = lookup(config, 'feature_file', 'PowwrDW.stage.Features.csv')

    future = PS.publish(TOPIC, message.encode('utf-8'),
                        TrainingSuppliers=TrainingSuppliers,
                        Supplier=Supplier,
                        ISO=ISO,
                        LoadZone=LoadZone,
                        Utility=Utility,
                        WeatherZone=WeatherZone,
                        WeatherStation=WeatherStation,
                        ExperimentID=ExperimentID,
                        MinDate=MinDate,
                        MaxDate=MaxDate,
                        ActualDelay=ActualDelay,
                        data_folder=data_folder,
                        MinLeaf=MinLeaf,
                        ActualDays=ActualDays,
                        Features=Features,
                        feature_file=feature_file)
    message_id = future.result()
    Track(message_id, 'prepare_model', 'create')


def lookup(config, name, default):
    return config[name] if name in config else default


def prepare_model(data, context):
    d = data
    print(d)

    # blows up if there are no attributes
    config = d['attributes']
    message = d['data']

    data_folder = lookup(config, 'data_folder', 'gs://stf-2020-data')
    df = pd.read_csv(os.path.join(data_folder, MAP))
    worthy = pd.read_csv(os.path.join(data_folder, WORTHY))

    Supplier_list = lookup(config, 'Supplier', 'DefaultSupplier').split('~')
    LoadZone_list = lookup(config, 'LoadZone', 'NORTH').split('~')
    Utility_list = lookup(config, 'Utility', 'ONCOR').split('~')
    WeatherZone_list = lookup(config, 'WeatherZone', 'NCENT').split('~')
    MinDate = lookup(config, 'MinDate', '20190101')
    MaxDate = lookup(config, 'MaxDate', '20191231')
    ActualDays_list = lookup(config, 'ActualDays', '120').split('~')

    n = 0
    bad = 0

    for LoadZone in LoadZone_list:
        for Utility in Utility_list:
            for WeatherZone in WeatherZone_list:
                stations = df[(df['WeatherZone'].str.lower()
                               == WeatherZone.lower())]
                for WeatherStation in stations['WeatherStation']:
                    for ActualDays in ActualDays_list:
                        for Supplier in Supplier_list:
                            isWorthy = check_worthy(
                                worthy, Supplier, LoadZone, Utility, WeatherZone)

                            if isWorthy == 0:
                                bad = bad + 1
                                print('debug', 'not worthy', Supplier,
                                      LoadZone, Utility, WeatherZone, WeatherStation)
                            else:
                                n = n + 1
                                print('debug', 'worthy', Supplier, LoadZone,
                                      Utility, WeatherZone, WeatherStation)
                                Publish_Model_request(config, message, Supplier, LoadZone,
                                                      Utility, WeatherZone, WeatherStation, MinDate, MaxDate, ActualDays, data_folder)

    print('publish', n, 'reject', bad)
