import os
import sys
import pandas
import pandasql


def aggregate_query(filename):

    df = pandas.read_csv(filename)
    df.rename(columns=lambda x: x.replace(' ', '_').lower(), inplace=True)

    q = 'select Supplier,Zone,substr(tomorrow,1,7) as MONTH,avg(ForecastMape) as Mape,count(*) AS Days'
    q = q + ',sum(case when ForecastMape < 5.0 then 1 else 0 end) AS Good'
    q = q + \
        ',sum(case when ForecastMape > 5.0 and ForecastMape < 10.0 then 1 else 0 end) AS Fair'
    q = q + ',sum(case when ForecastMape > 10.0 then 1 else 0 end) AS Poor'
    q = q + \
        ',Model,Knobs from df group by Model,Knobs,Supplier,Zone,substr(tomorrow,1,7)'

    return pandasql.sqldf(q.lower(), locals()).round(3)


csv_path = '/tsl/data/output'

csv_file = os.path.join(csv_path, "Model_Daily_Mape.csv")
df = aggregate_query(csv_file)

# print(df.sort_values(by=['mape']).head(20))
print(df.sort_values(by=['supplier', 'zone', 'month']))

out_file = os.path.join(csv_path, "model_monthly_Blowout.csv")
df.to_csv(out_file, index=False)
