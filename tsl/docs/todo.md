# To Do

This is the current todo list for the Short Term Accuracy project.

## Higher priority

- [ ] Create list of {Measure,Aggregation} pairs. ex: {'Temperature', 'avg'}{'mWh', 'sum'}
- [ ] Finalize input parameters to the python code.
  * [ ] Replace Year and Month with MinDate and MaxDate.
- [x] Save todo list as markdown
- [x] Print the tree, including questions (tmp <= 75) and equations ( z = ax + by + c )
- [ ] Compute score (3 daily mape numbers) and attach that to equation output attributes.
- [ ] Create an vwHourlyTimeSeries View (HourlyChronometer) with a cumulative hour column (daynumber * 24 + hour).
- [ ] Explain the challenges related to socializing many suppliers into a single model.
  * [ ] Who is the provider of the other supplier's Known and Unknown data?
  * [ ] We require Known data for Training.
  * [ ] We require Unknown data for Testing and Predicting.

## Lower priority

- [ ] Propose conventions for custom aggregates.
  * [ ] Temperature of the previous 6 hours : TMP_PREV_6
  * [ ] Previous(SKIP=0, TAKE=6)

- [x] Create a list of dimensions:
  * [x] Filters = dict({'Supplier': Supplier, 'LoadZone': LoadZone, 'Utility': Utility, 'WeatherZone': WeatherZone, 'WeatherStation': WeatherStation})
- [ ] Create a list of dimension hints. ex: {'ignore', 'compact', 'group by', 'socalize'}
- [ ] Document (*.md) table pictures of these concepts.
- [ ] Save the model as a pickle
- [ ] Allow for more models than just regression within trees.

- [ ] Create list of Dfsc_Internal data sources.
  * [ ] Actuals
  * [ ] Accuracy Details (for comparison, scoring)
  * [ ] Total System Load (PJM)
  * [ ] Weather by WeatherZone
  * [ ] WeatherData_raw.Forecast_ByUtility_PJM.csv
  * [ ] WeatherData_raw.Observed_ByUtility_PJM.csv
  * [ ] PJM_TotalSystemLoad_Forecast.csv
  * [ ] TotalSystemLoad.PJM_TotalSystemLoad.csv
  * [ ] DART Prices
  * [ ] Customer Count

maybe: Dimensions=Supplier:Clearview~LoadZone:NORTH~Utility:ONCOR,Features=Hour~Temperature:mean~Humidity:mean~CustomerCount:sum

features related to the calendar are aggregated as max, since these are attributes of a date/hour combination.
