# Google Cloud Plumbing - Short Term Model Creation

>---
>
>## Terms
>
>Here is a list of common terms and their definitions.
>
>---

| `Term` | Definition |
|:--|:--|
| `Topic` | A global todo list, or a blog, or a twitter account |
| `Subscription` | Following a blog or following on twitter. Each Topic can have many Subscriptions. |
| `Message` | An instance of data, with a collection of attributes. |
| `Publisher` | Writes a message to a topic. |
| `Subscriber` | Reads messages from a Subscription. |
| `Snapshot` | A bookmark in a Subscription. |
| | |

>---
>
>## Links
>
>Here is a list of useful links.
>
>---

|  | URL |
|:--|:--|
| `Our Functions` | <https://console.cloud.google.com/functions/list?project=husky-powwr-217818> |
| `Our Topics` | <https://console.cloud.google.com/cloudpubsub/topic/list?project=husky-powwr-217818> |
| `Our Subscriptions` | <https://console.cloud.google.com/cloudpubsub/subscription/list?project=husky-powwr-217818> |
| | |
| `Pub/Sub Documentation` | <https://cloud.google.com/pubsub/docs/publisher> |
| `Pub/Sub How To` | <https://cloud.google.com/pubsub/docs/how-to> |
| `Pub/Sub Diagram` | <https://cloud.google.com/pubsub/docs/quickstart-py-mac> |
| | |

>---
>
>## Sample Workflow
>
>We must request a model and save the prediction in a database.
>
>### Create Requests for models

~~~ cmd
gcloud pubsub topics publish ^
projects/husky-powwr-217818/topics/model-request ^
--attribute Supplier=APGE,Zone=PJM_AECO,Utility=ACE
~~~

>### Consume model predictions

~~~ cmd
Subscriber.exe -Sub watch-model-done -Minutes 5 -OutFile S:\STF\Model_predictions.csv
~~~

>### Load predictions into database

~~~ sql
BULK INSERT test.Model_predictions
  FROM 'S:\STF\Model_predictions.csv'
  WITH ( FIELDTERMINATOR = ',', FIRSTROW = 1, ROWTERMINATOR = '0x0a' )
~~~

>---
>
>## Detailed Flow
>
> This is the current flow.
>
> - A client produces a request for a model via publishing to the `model-request` topic.
> - The model is processed in the Google Cloud environment.
> - Subscribers consume the model predictions via the `watch-model-done` subscription.
>
> -  
>
>---

| This |Producer |   | Notifies |   | That | Consumer |
|--:|:--|:-:|:-:|:-:|--:|:--|
| Client | `User/App` |   | `publishes to` |   | Topic | `model-request` |
| Topic | `model-request` |   | `triggers` |   | Cloud Function | `prepare_model` |
| Cloud Function | `prepare_model` |   | `publishes to` |   | Topic | `model-todo` |
| Topic | `model-todo` |   | `triggers` |   | Cloud Function | `build_model` |
| Cloud Function | `build_model` |   | `publishes to` |   | Topic | `model-done` |
| Topic | `model-done` |   | `broadcasts to` |   | Subscription | `watch-model-done` |
| Subscription | `watch-model-done` |   | `consumed by` |   | Subscriber | `Subscriber.exe` |
| Subscriber | `Subscriber.exe` |   | `appends to` |   | File | `Model_predictions.csv` |
| | | | | | | |

>---
>
>## Subscriptions
>
>Here are the existing subscriptions.
>`watch-model-done` is the most important subscription.
>
>---

| Topic        | Subscription       | Message Retention Policy                |
|:-------------|:-------------------|:----------------------------------------|
| `model-done` | `watch-model-done` | Deletes message after acknowledgement.  |
| `model-done` | `log-model-done`   | Saves acknowledged messages for 7 days. |
| `model-todo` | `watch-model-todo` | Deletes message after acknowledgement.  |
| `model-todo` | `log-model-todo`   | Saves acknowledged messages for 7 days. |

>---
>
>## Screenshot of Publish
>
>This is a sample of using the Google web interface to publish a message to a topic.  
>In this example, all the information is passed through the attributes, and the message is blank.
>
>---

![Publish](Publish_Message.PNG)

>---
