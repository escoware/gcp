: TOPIC is constant
set TOPIC=projects/husky-powwr-217818/topics/model-request

: User-provided inputs
set SUPPLIER=Supplier=Clearview
set TRAININGSUPPLIERS=TrainingSuppliers=Clearview~APGE
set LOADZONE=LoadZone=NORTH
set UTILITY=Utility=ONCOR
set WEATHERZONE=WeatherZone=NCENT
set MINDATE=MinDate=20190101
set MAXDATE=MaxDate=20191231

: the experiment
set EXPERIMENTID=ExperimentID=2
set ACTUALDAYS=ActualDays=40
set FEATURES=Features=Hour~Temperature~DewPoint~Humidity

: run the experiment
call gcloud pubsub topics publish %TOPIC% --attribute %SUPPLIER%,%TRAININGSUPPLIERS%,%LOADZONE%,%UTILITY%,%WEATHERZONE%,%ACTUALDAYS%,%MINDATE%,%MAXDATE%,%FEATURES%,%EXPERIMENTID%

: define and run a second experiment

set EXPERIMENTID=ExperimentID=3
set ACTUALDAYS=ActualDays=30
set FEATURES=Features=Hour~Temperature~Humidity

: call gcloud pubsub topics publish %TOPIC% --attribute %SUPPLIER%,%TRAININGSUPPLIERS%,%LOADZONE%,%UTILITY%,%WEATHERZONE%,%ACTUALDAYS%,%MINDATE%,%MAXDATE%,%FEATURES%,%EXPERIMENTID%
