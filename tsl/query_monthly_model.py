import os
import sys
import pandas
import pandasql


def aggregate_query(filename):

    df = pandas.read_csv(filename)
    df.rename(columns=lambda x: x.replace(' ', '_').lower(), inplace=True)

    q = 'select Model,Knobs,Supplier,Zone,substr(tomorrow,1,7) as MONTH,avg(BackcastMape) AS BackcastMape,avg(ModelMape) as ModelMape,avg(ForecastMape) as ForecastMape,count(*) AS N from df group by Model,Knobs,Supplier,Zone,substr(tomorrow,1,7)'
    q = q + ' order by avg(ModelMape)'

    return pandasql.sqldf(q.lower(), locals()).round(3)


csv_path = '/tsl/data/output'
if len(sys.argv) > 1:
    csv_path = sys.argv[1]

csv_file = os.path.join(csv_path, "Model_Daily_Mape.csv")
df = aggregate_query(csv_file)

# print(df.sort_values(by=['forecastmape']).head(20))
print(df.sort_values(by=['model', 'supplier', 'zone', 'month']))

out_file = os.path.join(csv_path, "model_monthly_mape.csv")
df.to_csv(out_file, index=False)
