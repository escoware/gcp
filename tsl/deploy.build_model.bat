pushd \gcp\tsl\build_model
call gcloud functions deploy build_model ^
--project=husky-powwr-217818 ^
--entry-point build_model ^
--runtime python37 ^
--timeout=540 ^
--memory=1024MB ^
--trigger-topic model-todo ^
--max-instances=900 ^
--set-env-vars^
 PROJECT_ID=husky-powwr-217818^
,TOPIC_SUFFIX=model-done^
,TOPIC_RETRY=model-todo^
,TOPIC_TRACK=model-track^
,TOPIC_CODE=model-code^
,TOPIC_SQL=model-sql
popd
