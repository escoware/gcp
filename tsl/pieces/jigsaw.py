import sys
import os
import io
import csv
import pandas as pd


def jigsaw():

    # join performs a natural inner join based on matching column names
    def join(lhs, rhs):
        lhs_columns = set(lhs.columns.values)
        rhs_columns = set(rhs.columns.values)

        match_cols = list(lhs_columns.intersection(rhs_columns))
        if len(match_cols) == 0:
            print(lhs_columns, rhs_columns)

        df = pd.merge(lhs, rhs, on=match_cols)

        return df

    required = 1
    if len(sys.argv) <= required:
        print('To few arguments, please specify config file(s)')
        sys.exit(2)

    all = pd.read_csv(sys.argv[1])
    for i in range(2, len(sys.argv)):
        df = pd.read_csv(sys.argv[i])
        all = join(all, df)

    for c in all:
        all[c] = str(c)+'=' + all[c].astype('str')

    out = io.StringIO(newline='\r\n')
    all.to_csv(out, index=False, header=False)
    print(out.getvalue(), end='')


# Driver
if __name__ == "__main__":
    jigsaw()
