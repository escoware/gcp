import sys
import os
import io
import csv
import pandas as pd


def attributes2csv():

    for attributes in sys.stdin:
        print('attributes', attributes, len(attributes))
        if len(attributes) == 0:
            continue

        keys = []
        values = []
        arr = attributes.split(',')
        for a in arr:
            k, v = a.split('=')
            keys.append(k)
            values.append(v)

        print(','.join(keys))
        print(','.join(values))


# Driver
if __name__ == "__main__":
    attributes2csv()
