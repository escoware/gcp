import pandas as pd
import os
import logging
from writer import DataWriter
from feature_helper import FeatureUniverse


def filter_join(df, filterframe):
    # filter columns specified
    for key, frame in filterframe.items():
        if key in df.columns.values:
            df = join(df, pd.DataFrame({key: frame}))

    return df


def drop(df, filter):
    # drop columns specified
    for key, value in filter.items():
        if key in df.columns.values:
            df = df.drop([key], axis=1)

    return df


# join performs a natural inner join based on matching column names
def join(lhs, rhs):
    lhs_columns = set(lhs.columns.values)
    rhs_columns = set(rhs.columns.values)

    match_cols = list(lhs_columns.intersection(rhs_columns))
    df = pd.merge(lhs, rhs, on=match_cols)

    return df


def read_csv_filtered(folder, file_name, filter, date_name):
    logging.info('read file %s %s', folder, file_name)
    df = pd.read_csv(os.path.join(folder, file_name), delimiter=',',
                     parse_dates=[date_name])

    df = filter_join(df, filter)
    df = drop(df, filter)

    N, d = df.shape
    if N == 0:
        logging.warning('No Data %s %s %s', file_name,
                        filter, list(df.columns.values))
    return df, N


def log_no_data(w, filters, out, log):
    w.write_comment(Comment='No training data')
    print(str(filters), 'No training data', file=log, sep=',', end='')
    logging.info('data_O.shape %s', out.getvalue())


def join_all_data(data_folder, file_list, filters, date_name, lowest_date, highest_date, w, out, log):
    if len(file_list) == 0:
        data = pd.DataFrame(columns=[])
        return data

    for i in range(len(file_list)):
        file_name = 'PowwrDW.stage.{}.csv'.format(file_list[i])
        logging.debug('%d %s %s', i, file_list[i], file_name)

        df, N = read_csv_filtered(
            data_folder, file_name, filters, date_name)

        if N == 0:
            log_no_data(w, filters, out, log)

        if i == 0:
            data = df[(df[date_name] >= pd.Timestamp(lowest_date)) &
                      (df[date_name] <= pd.Timestamp(highest_date))]
        else:
            data = join(data, df)

    return data


def get_filters(config, event_id, out):

    ExperimentID = int(config['ExperimentID'])
    Supplier = config['Supplier']

    if 'TrainingSuppliers' in config:
        TrainingSuppliers = config['TrainingSuppliers'].split('~')
    else:
        TrainingSuppliers = Supplier.split('~')  # as an array

    LoadZone = config['LoadZone']
    Utility = config['Utility']
    WeatherZone = config['WeatherZone']
    WeatherStation = config['WeatherStation']

    train_filters = dict({'Supplier': TrainingSuppliers, 'LoadZone': [LoadZone],
                          'Utility': [Utility], 'WeatherZone': [WeatherZone], 'WeatherStation': [WeatherStation]})

    test_filters = dict({'Supplier': [Supplier], 'LoadZone': [LoadZone],
                         'Utility': [Utility], 'WeatherZone': [WeatherZone], 'WeatherStation': [WeatherStation]})

    # scope this better
    if 'MinLeaf' in config:
        MinLeaf = int(config['MinLeaf'])
    else:
        MinLeaf = 60

    ActualDays = int(config['ActualDays'])

    w = DataWriter(event_id, Supplier, LoadZone, Utility, WeatherZone,
                   WeatherStation, ExperimentID, MinLeaf, ActualDays, out)

    return train_filters, test_filters, w


def get_train_test_data(config, date_name, lowest_date, highest_date, event_id, out, log):

    data_folder = config['data_folder']
    feature_file = config['feature_file']

    feature_list = config['Features'].split('~')

    feature_key = 'Feature'
    f = FeatureUniverse(data_folder, feature_file, feature_key)
    all_features = f.all_features()
    logging.debug('all_features %s', all_features)

    train_filters, test_filters, w = get_filters(config, event_id, out)

    known, unknown = f.get_table_lists(feature_list)
    ignore_features = list(set(all_features).difference(set(feature_list)))

    # Train Known
    train_O = join_all_data(
        data_folder, known, train_filters, date_name, lowest_date, highest_date, w, out, log)

    # short circout
    N, d = train_O.shape
    if N == 0:
        test_O = train_O
        test_F = train_O
        return train_O, test_O, test_F, ignore_features, train_filters, w

    # Test Known
    test_O = join_all_data(
        data_folder, known, test_filters, date_name, lowest_date, highest_date, w, out, log)

    # Test Unknown
    test_F = join_all_data(data_folder, unknown,
                           test_filters, date_name, lowest_date, highest_date, w, out, log)

    logging.debug('train %d, test known %d, test unknown %d',
                  len(train_O), len(test_O), len(test_F))

    return train_O, test_O, test_F, ignore_features, train_filters, w
