"""

 model_tree.py  (author: Anson Wong / git: ankonzoid)

 Given a classification/regression model, this code builds its model tree.

"""
import io
import os
import logging
from ModelTree import ModelTree
from utils import load_csv_data
from data_helper import get_train_test_data
from datetime import datetime, timedelta
from datetime import date
from writer import DataWriter
from tree_details import log_tree_code, log_tree_sql, get_sql_parts
from retry import retry


def asDate(yyyymmdd):
    yyyy = int(yyyymmdd[:4])
    mm = int(yyyymmdd[4:6])
    dd = int(yyyymmdd[6:8])

    return date(yyyy, mm, dd)


def build_the_model(config, event_id, max_seconds):
    # ====================
    # Settings
    # ====================
    logging.basicConfig(level=logging.DEBUG)
    logging.debug('debug logging enabled')

    start_time = datetime.now()
    print(start_time)

    MinDate = config['MinDate']
    MaxDate = config['MaxDate']

    if 'MinLeaf' in config:
        MinLeaf = int(config['MinLeaf'])
    else:
        MinLeaf = 60

    if 'ActualDelay' in config:
        ActualDelay = int(config['ActualDelay'])
    else:
        ActualDelay = 7

    ActualDays = int(config['ActualDays'])

    date_name = 'Date'
    y_name = 'mWh'

    # ====================
    # Load data
    # ====================
    out = io.StringIO(newline='\r\n')
    log = io.StringIO(newline='\r\n')
    code = io.StringIO(newline='\n')
    sql = io.StringIO(newline='\r\n')
    print('# ', str(config), file=code)  # ensure the file is not empty
    print('-- ', str(config), file=sql)  # ensure the file is not empty

    lowest_date = asDate(MinDate) - timedelta(days=ActualDelay+ActualDays+1)
    highest_date = asDate(MaxDate)
    train_O, test_O, test_F, ignore_features, train_filters, w = get_train_test_data(
        config, date_name, lowest_date, highest_date, event_id, out, log)

    N, d = train_O.shape
    if N == 0:
        elapsed = (datetime.now() - start_time).seconds
        logging.info('%s %d Seconds. %s', 'fail', elapsed, log.getvalue())
        return out.getvalue(), log.getvalue(), code.getvalue(), sql.getvalue()

    N, d = test_F.shape
    if N == 0:
        elapsed = (datetime.now() - start_time).seconds
        logging.info('%s %d Seconds. %s', 'fail', elapsed, log.getvalue())
        return out.getvalue(), log.getvalue(), code.getvalue(), sql.getvalue()

    schema, x = get_sql_parts(config, None)

    print('CREATE SCHEMA', schema, '\nGO', file=sql)

    # ====================
    # for each day
    # ====================

    lo_date = asDate(MinDate)
    hi_date = asDate(MaxDate)
    min_date = train_O[date_name].min().date()
    max_date = train_O[date_name].max().date()

    if hi_date > max_date:
        logging.warning(
            'MaxDate = %s, Training data ends on %s', hi_date, max_date)
        hi_date = max_date

    if lo_date < min_date:
        logging.warning(
            'MinDate = %s, Training data starts on %s', lo_date, min_date)

    print(lo_date, hi_date, min_date, max_date)

    day_list = (lo_date + timedelta(days=x)
                for x in range(0, (hi_date-lo_date).days + 1))

    rows = 0
    mape_model = 0
    mape_backcast = 0
    mape_forecast = 0

    for tomorrow in day_list:
        day = tomorrow - timedelta(days=1)
        hi = day - timedelta(days=ActualDelay)
        lo = hi - timedelta(days=ActualDays)

        elapsed = (datetime.now() - start_time).seconds
        if elapsed > max_seconds:
            logging.warn('%s %d Seconds. %s', str(
                tomorrow), elapsed, 'Time Running Out')
            retry(config, tomorrow)
            break

        X, y, h, header, train_header = load_csv_data(
            train_O, lo, hi, ignore_features=ignore_features, date_name=date_name)

        if len(X) == 0:
            logging.info('%s %s', tomorrow, 'No Model Data')
            w.write_comment(day=tomorrow, Comment='No Model data')
            continue

        from linear_regr_mape import linear_regr_mape

        # Choose model
        model = linear_regr_mape(train_header, y_name=y_name)

        # Build model tree
        model_tree = ModelTree(model, header, min_samples_leaf=MinLeaf)

        # ====================
        # Train model tree
        # ====================
        model_tree.fit(X, y, verbose=False)
        y_pred = model_tree.predict(X)
        loss_train = model_tree.loss(X, y, y_pred)

        # ====================
        # Test model tree
        # ====================
        X_back, y_back, h, ignore, train_header = load_csv_data(
            test_O, tomorrow, tomorrow, ignore_features=ignore_features, date_name=date_name)
        if len(X_back) == 0:
            logging.info('%s %s', tomorrow, 'No Observed Data')
            w.write_comment(day=tomorrow, Comment='No Observed data')
            continue

        y_pred_O = model_tree.predict(X_back)
        loss_observed = model_tree.loss(X_back, y_back, y_pred_O)

        X_fore, y_fore, h, ignore, train_header = load_csv_data(
            test_F, tomorrow, tomorrow, ignore_features=ignore_features, date_name=date_name)

        if len(X_fore) == 0:
            logging.info('%s %s', tomorrow, 'No Forecast Data')
            w.write_comment(day=tomorrow, Comment='No Forecast data')
            continue

        y_pred_F = model_tree.predict(X_fore)
        loss_forecast = model_tree.loss(X_fore, y_fore, y_pred_F)

        for (h_i, y_O, y_F, y_A) in zip(h, y_pred_O, y_pred_F, y_back):
            error_Known = abs((y_A - y_O) / y_A) * 100 if y_A != 0 else 0.0
            error_Unknown = abs((y_A - y_F) / y_A) * 100 if y_A != 0 else 0.0
            error_Model = loss_train  # todo - compute hourly error
            w.write_row(tomorrow, h_i, y_O, y_F, y_A,
                        error_Known, error_Unknown, error_Model)

        hint = io.StringIO()
        print(tomorrow, str(train_filters), MinLeaf, ActualDays, len(X),
              "{:.3f},{:.3f},{:.3f}".format(loss_train, loss_forecast, loss_observed), file=hint, end='')

        elapsed = (datetime.now() - start_time).seconds
        print(elapsed, hint.getvalue())

        level = 0
        s = log_tree_code(model_tree.tree, level, train_header,
                          y_name, hint.getvalue())
        print(s, file=code)
        schema, function = get_sql_parts(config, tomorrow)
        s = log_tree_sql(model_tree.tree, level, train_header,
                         y_name, hint.getvalue(), schema, function)
        print(s, file=sql)

        rows = rows + 1
        mape_model = mape_model + loss_train
        mape_backcast = mape_backcast + loss_observed
        mape_forecast = mape_forecast + loss_forecast

    if rows == 0:
        w.write_comment(Comment='No forecast data')
        print('No forecast data', file=log, sep=',', end='')
        logging.info('%s %s', 'rows', out.getvalue())
        elapsed = (datetime.now() - start_time).seconds
        logging.info('%s %d Seconds. %s', 'fail', elapsed, log.getvalue())
        return out.getvalue(), log.getvalue(), code.getvalue(), sql.getvalue()

    w.bq_flush()
    gap = (mape_backcast - mape_model) / rows
    elapsed = (datetime.now() - start_time).seconds
    print(lo_date, hi_date, max_date,
          "success {} Seconds. gap = {:.3f} leaf={} rows={} days={} {:.3f},{:.3f},{:.3f}".format(elapsed, gap, MinLeaf, rows, ActualDays, mape_model / rows, mape_forecast / rows, mape_backcast / rows), file=log, sep=',', end='')

    logging.debug('%s', log.getvalue())

    return out.getvalue(), log.getvalue(), code.getvalue(), sql.getvalue()
