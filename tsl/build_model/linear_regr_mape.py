"""

 linear_regr.py  (author: Anson Wong / git: ankonzoid)

"""
import numpy as np
from sklearn.metrics import mean_squared_error
import warnings
warnings.filterwarnings(action="ignore", module="scipy",
                        message="^internal gelsd")


class linear_regr_mape:

    def __init__(self, header, y_name):
        from sklearn.linear_model import LinearRegression
        self.model = LinearRegression()
        self.header = header
        self.y_name = y_name

    def fit(self, X, y):
        self.model.fit(X, y)

    def predict(self, X):
        return self.model.predict(X)

    def loss(self, X, y, y_pred):
        return np.mean(np.abs((y - y_pred) / y)) * 100  # MAPE

    def coef_(self):
        return list(self.model.coef_)

    def intercept_(self):
        return self.model.intercept_

    def equation(self, prefix):  # prefix=@
        return "{:6.6f}{}".format(self.model.intercept_, ''.join([" + ({:6.6f} * @{})".format(num, name) for (num, name) in zip(self.model.coef_, self.header)]))

    def __repr__(self):
        class_name = self.__class__.__name__
        return "{} = {:6.6f}{}".format(self.y_name, self.model.intercept_, ''.join([" + ({:6.6f} * {})".format(num, name) for (num, name) in zip(self.model.coef_, self.header)]))
