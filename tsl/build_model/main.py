import base64
import os
import io
import pickle
import logging
import gcsfs
import pandas as pd
from datetime import datetime
from model import build_the_model

from google.cloud import storage
from google.cloud import pubsub_v1

CS = storage.Client()
PS = pubsub_v1.PublisherClient()
logging.basicConfig(level=logging.DEBUG)


def build_model(data, context):
    '''This function is executed from a Cloud Pub/Sub'''
    message = base64.b64decode(data['data']).decode('utf-8')
    config = data['attributes']
    logging.info('%s', config)

    event_id = context.event_id

    PROJECT_ID = os.getenv('PROJECT_ID')
    TOPIC_TRACK = os.getenv('TOPIC_TRACK')
    TRACK = 'projects/%s/topics/%s' % (PROJECT_ID, TOPIC_TRACK)

    def Track(message_id, my_name, status):
        # when is more important than who
        asof = str(datetime.now().timestamp())
        message = 'track,{},{},{}\n'.format(message_id, asof, status)
        print(message)
        PS.publish(TRACK, message.encode('utf-8'))

    Track(event_id, 'build_model', 'submit')
    max_seconds = 500
    out, log, code, sql = build_the_model(config, event_id, max_seconds)
    Track(event_id, 'build_model', 'complete')

    TOPIC_SUFFIX = os.getenv('TOPIC_SUFFIX')
    TOPIC = 'projects/%s/topics/%s' % (PROJECT_ID, TOPIC_SUFFIX)

    bytestring = out.encode('UTF-8', errors='strict')
    PS.publish(TOPIC, bytestring, log=log, message=message, event_id=event_id)

    #

    TOPIC_CODE = os.getenv('TOPIC_CODE')
    TOPIC = 'projects/%s/topics/%s' % (PROJECT_ID, TOPIC_CODE)

    bytestring = code.encode('UTF-8', errors='strict')
    PS.publish(TOPIC, bytestring, event_id=event_id)
    #

    TOPIC_SQL = os.getenv('TOPIC_SQL')
    TOPIC = 'projects/%s/topics/%s' % (PROJECT_ID, TOPIC_SQL)

    bytestring = sql.encode('UTF-8', errors='strict')
    PS.publish(TOPIC, bytestring, event_id=event_id)
