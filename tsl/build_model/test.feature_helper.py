import sys
import os
import csv
import collections
from feature_helper import FeatureUniverse


def main():
    # ====================
    # Settings
    # ====================

    if len(sys.argv) < 4:
        print('To few arguments, please specify folder, file, column')
        sys.exit(2)

    all_features = sys.argv[1]
    key = 'Feature'
    f = FeatureUniverse(sys.argv[1], sys.argv[2], key)

    print(f.all_features())

    features = sys.argv[3].split('~')

    known, unknown = f.get_table_lists(features)

    for i in range(len(known)):
        if i == 0:
            print('first is', known[i])
        else:
            print('join', known[i-1], known[i])


# Driver
if __name__ == "__main__":
    main()
