import os
from datetime import datetime
import pandas as pd


def retry(config, tomorrow):
    # tomorrow is the first day we did not process

    PROJECT_ID = os.getenv('PROJECT_ID')
    TOPIC_RETRY = os.getenv('TOPIC_RETRY')
    RETRY = 'projects/%s/topics/%s' % (PROJECT_ID, TOPIC_RETRY)
    message = 'retry'

    from google.cloud import pubsub_v1
    PS = pubsub_v1.PublisherClient()

    PS.publish(RETRY, message.encode('utf-8'),
               TrainingSuppliers=str(config['TrainingSuppliers']),
               Supplier=str(config['Supplier']),
               ISO=str(config['ISO']),
               LoadZone=str(config['LoadZone']),
               Utility=str(config['Utility']),
               WeatherZone=str(config['WeatherZone']),
               WeatherStation=str(config['WeatherStation']),
               ExperimentID=str(config['ExperimentID']),
               MinDate=str(tomorrow).replace('-', ''),
               MaxDate=str(config['MaxDate']),
               ActualDelay=str(config['ActualDelay']),
               data_folder=str(config['data_folder']),
               MinLeaf=str(config['MinLeaf']),
               ActualDays=str(config['ActualDays']),
               Features=str(config['Features']),
               feature_file=str(config['feature_file']))
