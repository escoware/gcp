import os
import logging
import pandas as pd
from collections import OrderedDict


class FeatureUniverse(object):

    def __init__(self, folder_name, file_name, key):

        self.folder_name = folder_name
        self.file_name = file_name
        self.key = key
        self.df = pd.DataFrame({})

        self.df = pd.read_csv(os.path.join(
            folder_name, file_name), delimiter=',')

    def all_features(self):
        f = self.df[self.key]
        return list(f.values)

    def get_table_lists(self, features):
        errors = 0

        od_known = OrderedDict()
        od_unknown = OrderedDict()
        for f in features:
            row = self.df[(self.df[self.key].str.lower() == f.lower())]
            if len(row) != 1:
                errors = errors + 1
                logging.warning(
                    'Unexpected Feature %s, %d rows', f, len(row))
                continue

            k = row['KnownSource'].values[0]
            u = row['UnknownSource'].values[0]
            if u != u:
                u = k
            od_known[k] = 1
            od_unknown[u] = 1

        if errors > 0:
            return list([]), list([])

        return list(od_known.keys()), list(od_unknown.keys())
