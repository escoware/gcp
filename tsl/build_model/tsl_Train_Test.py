import sys
import os
import csv
import collections
from model import build_the_model


def main():
    # ====================
    # Settings
    # ====================

    required = 0
    if len(sys.argv) <= required:
        print('To few arguments, please specify Supplier, and config file(s)')
        sys.exit(2)

    #Supplier = sys.argv[1]

    config = collections.ChainMap()

    for i in range(len(sys.argv)-1, required, -1):
        for row in csv.DictReader(open(sys.argv[i])):
            config = config.new_child(row)

    event_id = 123
    max_seconds = 3600
    rc = build_the_model(config, event_id, max_seconds)

    output_folder = config['output_folder']
    Results_file = config['Results_file']
    Prediction_file = config['Prediction_file']
    Equation_file = config['Equation_file']
    SQL_file = config['SQL_file']

    out, log, code, sql = rc
    with open(os.path.join(output_folder, Results_file), "a") as f:
        print(log, file=f)

    with open(os.path.join(output_folder, Equation_file), "a") as f:
        print(code, file=f)

    with open(os.path.join(output_folder, SQL_file), "a") as f:
        print(sql, file=f)

    with open(os.path.join(output_folder, Prediction_file), "a") as f:
        print(out, file=f)


# Driver
if __name__ == "__main__":
    main()
