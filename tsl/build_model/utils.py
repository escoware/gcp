"""

 utils.py  (author: Anson Wong / git: ankonzoid)

"""
import numpy as np
import pandas as pd


def load_csv_data(alldata, lo, hi, mode="regr", verbose=False, ignore_features=[], date_name='Date', y_name='mWh', hour_name='Hour'):
    # extract X by dropping y column
    df = alldata[(alldata[date_name] >= pd.Timestamp(lo)) & (
        alldata[date_name] <= pd.Timestamp(hi)) & (alldata[y_name] != 0)].drop([date_name], axis=1)

    my_ignore_features = list(
        set(ignore_features).intersection(set(df.columns.values)))

    df = df.drop(my_ignore_features, axis=1)

    df_header = df.columns.values  # header
    header = list(df_header)
    N, d = len(df), len(df_header) - 1

    X = np.array(df.drop([y_name], axis=1))
    y = np.array(df[y_name])  # extract y
    h = np.array(df[hour_name])  # extract hour
    y_classes = list(set(y))

    assert X.shape == (N, d)  # check X.shape
    assert y.shape == (N,)  # check y.shape
    if mode == "clf":
        assert y.dtype in ['int64']  # check y are integers
    elif mode == "regr":
        assert y.dtype in ['int64', 'float64']  # check y are integers/floats
    else:
        exit("err: invalid mode given!")
    if verbose:
        print(" header={}\n X.shape={}\n y.shape={}\n len(y_classes)={}\n".format(
            header, X.shape, y.shape, len(y_classes)))

    train_header = list(df.drop([y_name], axis=1).columns.values)
    return X, y, h, header, train_header
