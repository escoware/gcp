def get_sql_parts(config, Tomorrow):
    YYYYMMDD = str(Tomorrow).replace('-', '')
    sql_schema = 'Experiment_' + config['ExperimentID']
    skeleton = '{}_' + YYYYMMDD + '_ActualDays_{}_MinLeaf_{}'
    ActualDays = config['ActualDays']

    if 'TrainingSuppliers' in config:
        TrainingSuppliers = config['TrainingSuppliers']  # with tildes ??
    else:
        TrainingSuppliers = config['Supplier']

    if 'MinLeaf' in config:
        MinLeaf = config['MinLeaf']
    else:
        MinLeaf = '60'
    sql_function = skeleton.format(TrainingSuppliers, ActualDays, MinLeaf)

    return sql_schema, sql_function


def log_tree_code(node, level, header, y_name=None, hint=None):
    if level == 0:
        rc = '# hint - ' + hint + '\n'
        rc = rc + 'Predict('+', '.join(header)+'):' + '\n'
        rc = rc + log_tree_code(node, level+1, header) + '\n'
        rc = rc + '    return ' + y_name + '\n' + '\n' + '\n'
        return rc
    else:
        no_children = node["children"]["left"] is None and node["children"]["right"] is None
        indent = "".join(["    "] * level)
        if no_children:
            return indent + str(node["model"]) + ' # MAPE = {:.3f} ({})'.format(node["loss"], node["n_samples"]) + '\n'
        else:
            rc = indent + 'if ' + header[node["j_feature"]
                                         ] + ' <= ' + '{:.3f}'.format(node["threshold"]) + ':' + ' # MAPE = {:.3f} ({})'.format(node["loss"], node["n_samples"]) + '\n'
            rc = rc + log_tree_code(node["children"]["left"], level+1, header)
            rc = rc + indent + 'else\n'
            rc = rc + log_tree_code(node["children"]["right"], level+1, header)
            return rc


def log_tree_sql(node, level, header, y_name, hint=None, schema=None, function=None):
    if level == 0:
        rc = '-- hint - ' + hint + '\n'
        rc = rc + 'CREATE FUNCTION ' + schema + '.' + function
        rc = rc + '('+', '.join(["@{} float".format(name)
                                 for name in header])+')' + '\n'
        rc = rc + 'RETURNS TABLE AS RETURN (' + '\n'
        rc = rc + 'SELECT ' + y_name + ' =' + '\n'
        rc = rc + log_tree_sql(node, level+1, header, y_name) + '\n'
        rc = rc + ')' + '\n' + 'GO' + '\n'
        return rc

    else:
        no_children = node["children"]["left"] is None and node["children"]["right"] is None
        indent = "".join(["    "] * level)
        if no_children:
            # y = mx + b -- mape
            s = indent + node["model"].equation('@')
            s = s + ' -- MAPE = {:.3f}'.format(node["loss"])
            s = s + ' ({})'.format(node["n_samples"]) + '\n'
            return s
        else:
            lhs = log_tree_sql(node["children"]["left"],
                               level+1, header, y_name)
            rhs = log_tree_sql(node["children"]["right"],
                               level+1, header, y_name)
            test = '@'+header[node["j_feature"]] + \
                ' <= ' + '{:.3f}'.format(node["threshold"])
            test = test + ' -- MAPE = {:.3f}'.format(node["loss"])
            test = test + ' ({})'.format(node["n_samples"])

            rc = ''
            rc = rc + indent + 'CASE\n'
            rc = rc + indent + 'WHEN ' + test + '\n'
            rc = rc + indent + 'THEN' + '\n'
            rc = rc + lhs
            rc = rc + indent + 'ELSE\n'
            rc = rc + rhs
            rc = rc + indent + 'END\n'

            return rc
