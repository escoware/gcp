

class DataWriter(object):
    # outfile shape
    # event_id,Supplier,LoadZone,Utility,WeatherZone,WeatherStation,CalendarDate,Hour,ExperimentID,mWh_Known,mWh_Unknown,mWh_Actual,MinLeaf,ActualDays,error_Known,error_Unknown,error_Model,Comment

    def __init__(self, event_id, Supplier, LoadZone, Utility, WeatherZone, WeatherStation, ExperimentID, MinLeaf, ActualDays, out):

        self.event_id = event_id
        self.Supplier = Supplier
        self.LoadZone = LoadZone
        self.Utility = Utility
        self.WeatherZone = WeatherZone
        self.WeatherStation = WeatherStation
        self.ExperimentID = ExperimentID
        self.MinLeaf = MinLeaf
        self.ActualDays = ActualDays
        self.out = out
        self.rows = []

    def bq_flush(self):
        from google.cloud import bigquery
        bqc = bigquery.Client()
        table_ref = bqc.dataset('STFA').table('Model_predictions')
        table = bqc.get_table(table_ref)
        errors = bqc.insert_rows(table, self.rows)  # API request
        assert errors == []
        self.rows = []

        return self

    def write_row(self, day, hour, mWh_Known, mWh_Unknown, mWh_Actual, error_Known, error_Unknown, error_Model, Comment=''):
        print(self.event_id, self.Supplier, self.LoadZone, self.Utility, self.WeatherZone, self.WeatherStation, day, hour, self.ExperimentID, "{:.6f},{:.6f},{:.6f}".format(
            float(mWh_Known), float(mWh_Unknown), float(mWh_Actual)), self.MinLeaf, self.ActualDays, "{:.6f},{:.6f},{:.6f}".format(
            float(error_Known), float(error_Unknown), float(error_Model)), Comment, file=self.out, sep=',')
        row_tup = (str(self.event_id), self.Supplier, self.LoadZone, self.Utility, self.WeatherZone, self.WeatherStation, str(day), str(hour), str(
            self.ExperimentID), mWh_Known, mWh_Unknown, mWh_Actual, self.MinLeaf, self.ActualDays, error_Known, error_Unknown, error_Model, Comment)
        self.rows.append(row_tup)

        return self

    def write_comment(self, day='2001-01-01', hour=0, Comment=''):
        error_Known = ''
        error_Unknown = ''
        error_Model = ''
        mWh_Known = '0.0'
        mWh_Unknown = '0.0'
        mWh_Actual = '0.0'

        print(self.event_id, self.Supplier, self.LoadZone, self.Utility, self.WeatherZone, self.WeatherStation, day, hour, self.ExperimentID, "{:.6f},{:.6f},{:.6f}".format(
            float(mWh_Known), float(mWh_Unknown), float(mWh_Actual)), self.MinLeaf, self.ActualDays, error_Known, error_Unknown, error_Model, Comment, file=self.out, sep=',')

        return self

    def __repr__(self):
        class_name = self.__class__.__name__
        return "{}({})".format(class_name, ', '.join(["{}={}".format(k, v) for k, v in self.get_params(deep=False).items()]))
