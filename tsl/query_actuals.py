import os
import sys
import pandas
import pandasql


def aggregate_query(df, keys):

    q = 'select '+keys+',avg(mWh) AS MW,count(*) AS N from df group by '+keys
    q = q + ' order by avg(mWh)'

    return pandasql.sqldf(q.lower(), locals()).round(3)


csv_path = 'D:\\stuff\\csv'

csv_file = 'PowwrDW.LoadActualSupplier.csv'

csv_file = os.path.join(csv_path, csv_file)
df_in = pandas.read_csv(csv_file)
df_in.rename(columns=lambda x: x.replace(' ', '_').lower(), inplace=True)
print('OK')

from collections import OrderedDict

keys = sys.argv[1].split('~') # loadzone~supplier~utility
aggs = sys.argv[2].split('~') # mwh:sum~hour:max

agg_map = OrderedDict()
for v in aggs:
    a = v.split(':')
    if len(a) == 1:
        agg_map[a[0]] = 'max'
    else:
        agg_map[a[0]] = a[1]

print(keys)
print(aggs)

df = df_in.groupby(keys, as_index=False).agg(agg_map)
print(df.sort_values(by=['mwh'], ascending=False).head(10))

df.to_csv('Summary.csv', index=False)
