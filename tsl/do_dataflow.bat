: gcloud dataflow jobs run asdf
: ERROR: (gcloud.dataflow.jobs.run) argument --gcs-location: Must be specified.
: Usage: gcloud dataflow jobs run JOB_NAME --gcs-location=GCS_LOCATION [optional flags]
:   optional flags may be  --disable-public-ips | --help | --max-workers |
:                          --parameters | --region | --service-account-email |
:                          --staging-location | --zone

: figure out ,machineType=n1-standard-1

: Expected usage:
: do_dataflow model-done csv
: do_dataflow model-code py
: do_dataflow model-sql sql


: %1 = short topic {model-done, model-code, model-sql}
: %2 = suffix {csv, py, sql}

set SHORT_TOPIC=%1
set TOPIC=projects/husky-powwr-217818/topics/%SHORT_TOPIC%
set JOB_NAME=watch-%SHORT_TOPIC%
set JOB_TEMPLATE=gs://dataflow-templates/latest/Cloud_PubSub_to_GCS_Text
set OUT_DIR=gs://stf-2020-audit/%2/
set PREFIX=Model_predictions.
set SUFFIX=.%2

zgcloud dataflow jobs run %JOB_NAME% --gcs-location %JOB_TEMPLATE% ^
--max-workers=1 ^
--region=us-central1 ^
--parameters inputTopic=%TOPIC%,outputDirectory=%OUT_DIR%,outputFilenamePrefix=%PREFIX%,outputFilenameSuffix=%SUFFIX%



set JOB_NAME=watch-model-sql
set TOPIC=projects/husky-powwr-217818/topics/model-sql
set OUT_DIR=gs://stf-2020-audit/sql/
set PREFIX=Model_predictions.
temp: gs://stf-2020-audit/tmp
workers=1
zone=us-east1-b
machine=n1-standard-1

set JOB_NAME=watch-model-done
set TOPIC=projects/husky-powwr-217818/topics/model-done
set JOB_TEMPLATE=gs://dataflow-templates/latest/Cloud_PubSub_to_GCS_Text
set OUT_DIR=gs://stf-2020-audit/csv/
set PREFIX=Model_predictions.
set SUFFIX=.csv

machine type = g1-small (too small)
us-east1-b

temp: gs://stf-2020-audit/tmp

