pushd \gcp\tsl\prepare_model
call gcloud functions deploy prepare_model ^
--project=husky-powwr-217818 ^
--entry-point prepare_model ^
--runtime python37 ^
--timeout=540 ^
--trigger-topic model-request ^
--set-env-vars TOPIC=projects/husky-powwr-217818/topics/model-todo,MAP=PowwrDW.stage.WeatherMap.csv,WORTHY=PowwrDW.stage.Worthy.csv,TRACK=projects/husky-powwr-217818/topics/model-track
popd
